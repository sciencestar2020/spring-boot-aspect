package com.example.springaspect.model;

public class Order {
    private long orderId;
    private String createTime;
    public long getOrderId() {
        return orderId;
    }
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    @Override
    public String toString() {
        return "Order [orderId=" + orderId + ", createTime=" + createTime + "]";
    }
}
