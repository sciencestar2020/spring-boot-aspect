package com.example.springaspect.controller;

import com.example.springaspect.model.RespHead;
import com.example.springaspect.model.Result;
import com.example.springaspect.model.User;
import com.example.springaspect.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/getUser")
    public Result<User> getUser(User user){
        System.out.println("#####################################");
        user=userService.getUser(user);
        Result<User> result =new Result<User>();
        RespHead respHead=new RespHead();
        result.setRespHead(respHead);
        result.setContent(user);
        return result;
    }
}
