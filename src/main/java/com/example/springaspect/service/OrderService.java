package com.example.springaspect.service;

import com.example.springaspect.model.Order;
import com.example.springaspect.utils.CommUtils;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    public Order getOrder(Long orderId){
        Order order =new Order();
        order.setOrderId(orderId);
        order.setCreateTime(CommUtils.getTimeStr());
        return order;
    }
}
